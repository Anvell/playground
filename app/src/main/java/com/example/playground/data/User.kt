package com.example.playground.data

data class User(
    val username: String,
    val score: Int = 42
)
