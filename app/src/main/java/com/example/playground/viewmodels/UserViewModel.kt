package com.example.playground.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.playground.data.User

class UserViewModel : ViewModel() {
    private val user = MutableLiveData(User("Adam"))

    fun getUser(): LiveData<User> = user

    fun incrementScore() {
        val currentScore = user.value?.score ?: 0
        user.value = user.value?.copy(score = currentScore + 1)
    }
}
